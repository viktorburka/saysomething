package saysomething

import (
    "fmt"
    "rsc.io/quote"
)

func SayIt(to string) {
    fmt.Println(to, quote.Go())
}
